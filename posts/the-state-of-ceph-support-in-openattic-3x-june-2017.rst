.. title: Update on the State of Ceph Support in openATTIC 3.x (June 2017)
.. slug: the-state-of-ceph-support-in-openattic-3x-june-2017
.. date: 2017-06-11 13:39:01 UTC+02:00
.. tags: ceph, development, management, update
.. category: 
.. link: 
.. description: Summarizing the latest Ceph management features in oA 3.x
.. type: text
.. author: Lenz Grimmer

A bit over a month ago, I posted about a few :doc:`new Ceph management features
<sneak-preview-upcoming-ceph-management-features>` that we have been working on
in openATTIC 3.x after we finished :doc:`refactoring the code base
<implementing-a-more-scalable-storage-management-framework-in-openattic-30>`.

These have been merged into the trunk in the meanwhile, and the team has started
working on additional features. In this post, I'd like to give you an update on
the latest developments and share some screen shots with you.

.. TEASER_END

iSCSI Target Management
-----------------------

The management and configuration of iSCSI targets (:issue:`OP-775`) was released
in oA 3.0.0. This feature uses Ceph RADOS block devices (RBDs) as the backing
store and `lrbd <https://github.com/SUSE/lrbd>`_ for the target configuration on
the remote iSCSI storage nodes; which in turn is orchestrated by our Salt-based
Ceph deployment and configuration framework `DeepSea
<https://github.com/SUSE/DeepSea>`_. This way, openATTIC is now capable of
managing iSCSI targets on multiple nodes, supporting a wide range of features
and authentication methods.

.. slides::

    /galleries/oa-3.x-preview-2017-06/oa-3.x-iscsi-target.png
    /galleries/oa-3.x-preview-2017-06/oa-3.x-iscsi-target-detail.png

While openATTIC 2.x had to be installed on the same node on which the Salt
master resided, this limitation has now been lifted in version 3.x - openATTIC
now uses the `Salt REST API
<http://salt.readthedocs.io/projects/salt-api/en/latest/ref/netapis/all/saltapi.netapi.rest_cherrypy.html>`_
to communicate with the Salt master instead. The Salt management code in
the openATTIC backend was refactored accordingly to support this.

Object Gateway Management
-------------------------

The `Ceph Object Gateway <http://docs.ceph.com/docs/master/radosgw/>`_ (aka
RADOS Gateway/RGW) is an object store compatible with the Amazon S3 and
OpenStack Swift protocol. It uses the Ceph cluster as its storage backend.

RGW maintains it's own user database that manages the user accounts, their
privileges, quotas and access keys.

.. slides::

    /galleries/oa-3.x-preview-2017-06/oa-3.x-rgw-user.png
    /galleries/oa-3.x-preview-2017-06/oa-3.x-rgw-s3_key.png

We added initial RADOS Gateway user and key management support in openATTIC
3.1.0. User quota management (:issue:`OP-766`) landed in oA 3.1.1, managing a
user's buckets is scheduled for the next release.

The openATTIC backend acts as a proxy between the the `RGW Admin Ops API
<http://docs.ceph.com/docs/master/radosgw/adminops/>`_ and the openATTIC
web-based UI.

Ceph Dashboard Improvements
---------------------------

openATTIC 3.x added some additional functionality to the Ceph cluster dashboard,
namely the addition of an OSD health widget (:issue:`OP-1937`) and a number of
usability improvements to the main cluster health widget: it now provides more
detailed insight into the cluster's health state. A widget to monitor the status
of the Ceph MONs (:issue:`OP-1933`) is also nearing completion and will be added
to the next release.

.. slides::

    /galleries/oa-3.x-preview-2017-06/oa-3.x-dashboard-widgets.png
    /galleries/oa-3.x-preview-2017-06/oa-3.x-osd-widget.png
    /galleries/oa-3.x-preview-2017-06/oa-3.x-osd-widget-2.png
    /galleries/oa-3.x-preview-2017-06/oa-3.x-mon-widget.gif

In addition to these built-in widgets, we're also working on integrating
`Prometheus <https://prometheus.io/>`_ for monitoring and `Grafana
<https://grafana.com/>`_ for visualization of the various Ceph related data and
information.

Other UI Improvements
---------------------

The oA UI also received some overall usablity enhancements. For example, it now
displays a warning about unsaved changes in a form (:issue:`OP-1732`) and
collects all recently displayed notifications, in case you have missed them
(:issue:`OP-1989`). Both these enhancements were contributed by Tiago Melo
(thank you!).

.. slides::

    /galleries/oa-3.x-preview-2017-06/oa-3.x-unsaved-changes.png
    /galleries/oa-3.x-preview-2017-06/oa-3.x-recent-notifications.png

Availability
------------

Currently, openATTIC 3.x is available in packaged form for `openSUSE Leap
<https://www.opensuse.org/>`_ via the `filesystems:openATTIC:3.x/openattic
<https://build.opensuse.org/project/show/filesystems:openATTIC:3.x/openattic/>`_
package repository on the `openSUSE Build Service
<https://build.opensuse.org/>`_. We plan to add packages for other distributions
as well (e.g. CentOS, Debian/Ubuntu), this is work in progress and it also
depends on the availability of DeepSea for these platforms (which is also work
in progress). The DeepSea developers are also looking for more testers and
feedback!

As usual, we appreciate any feedback or comments you might have - please
:doc:`get in touch with us <get-involved>`!