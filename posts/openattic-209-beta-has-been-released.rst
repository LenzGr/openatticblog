.. title: openATTIC 2.0.9 beta has been released
.. slug: openattic-209-beta-has-been-released
.. date: 2016-03-16 17:28:29 UTC+01:00
.. tags: announcement, collaboration, contributing, news, beta, opensource, release
.. category: 
.. link: 
.. description: Announcing the openATTIC 2.0.9 beta release
.. type: text
.. author: Lenz Grimmer

With a somewhat shorter release interval than usual and just in time for
`Chemnitzer Linuxtage <https://chemnitzer.linux-tage.de/2016/en>`_, we would
like to announce that a new beta version 2.0.9 of openATTIC has now been
published and is available for download.

This release fixes an issue that caused ``oaconfig install`` to not perform
all necessary all installation steps when run the first time.

It also includes a number of installation and portability fixes to the Nagios
configuration and plugins (thanks to Erick Jackson from SUSE for contributing
these!).

In addition to that, a lot of improvements were made to the RPM packages for
RHEL7 and derivative distributions.

We also updated and restructured the `installation instructions
<http://docs.openattic.org/2.0/install_guides/index.html>`_ in the
documentation in order to make the installation more straightforward.

.. TEASER_END

Please note that 2.0.9 is still a **beta version**. As usual, handle with care
and make sure to create backups of your data!

We would like to thank everyone who contributed to this release! Special
thanks to the folks from SUSE for their help and support.

Your feedback, ideas and bug reports are very welcome. If you would like to
get in touch with us, consider joining our `openATTIC Users
<https://groups.google.com/forum/#%21forum/openattic-users>`_ Google Group,
visit our `#openattic <irc://irc.freenode.org/#openattic>`_ channel on
`irc.freenode.net <http://webchat.freenode.net/?channels=openattic>`_ or leave
comments below this blog post.

See the list below for a more detailed change log and further references. The
OP codes in brackets refer to individual Jira issues that provide additional
details on each item. You can review these on our `public Jira instance
<http://tracker.openattic.org/>`_.

Changelog 2.0.9

* WebUI: Review licenses of bower packages and NPM modules (:issue:`OP-857`)
* WebUI: Update versions of bower dependencies (:issue:`OP-904`)
* WebUI: Update versions of npm packages (:issue:`OP-906`)
* WebUI/QA: Refactored "volumes add" E2E tests (:issue:`OP-921`)
* Installation: Removed "oaconfig install <package>" functionality, as it
  wasn't portable and package installation should rather be performed by
  the dedicated OS tools (e.g. apt-get or yum) (:issue:`OP-659`)
* Installation: Fixed error in "oaconfig install" that resulted in an
  incomplete installation ("This command requires oacli to be
  configured") (:issue:`OP-950`)
* Monitoring: Removed hard-code path names in the openATTIC Nagios plugins,
  created distribution-specific configuration files and added configuration
  options for Nagios-specific settings (:issue:`OP-842`) (thanks to Eric
  Jackson for the patch)
* Installation: The Nagios plugins now use the distribution's default
  directory /var/lib/nagios on EL7 to store runtime information (as
  configured via the setting NAGIOS_STATE_DIR in
  /etc/(default|sysconfig)/openattic) instead of /var/lib/nagios3 (which is
  the default in Debian). Removed directory /var/lib/nagios3 from the
  openattic-module-nagios RPM package.
* Installation: Added missing package dependencies (bc, nagios-plugins-tcp)
  to the openattic-plugin-monitoring RPM so that the "check_openattic_*"
  Nagios plugins actually work on EL7 (:issue:`OP-976`, :issue:`OP-977`)
* Installation: The Nagios plugins now read /etc/(default|sysconfig)/openattic
  to obtain distribution-specific configuration options (esp. path names).
  This makes them more portable across distributions and resolves most the
  issues observed on EL7 and SLES12 (except for the pnp4nagios configuration,
  which still requires manual intervention on EL7) (:issue:`OP-820`).
* Installation: added tmpfiles.d/openattic.conf to the RPM package to
  re-create /var/lock/openattic after a reboot (:issue:`OP-738`)
* Installation: Added missing pnp4nagios config files to openattic-module-
  nagios RPM (:issue:`OP-983`)
* Installation: Added /etc/openattic to the openattic-base RPM
  (:issue:`OP-982`)
* Documentation: Improved the EL7 installation instructions, added note 
  about pnp4nagios configuration (:issue:`OP-981`)
* Documentation: Restructured and refined the installation instructions
* Documentation: Added trade mark notice
