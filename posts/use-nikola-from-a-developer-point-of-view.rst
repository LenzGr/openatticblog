.. title: Use Nikola from a developer point of view
.. slug: use-nikola-from-a-developer-point-of-view
.. date: 2017-04-10 11:01:35 UTC+02:00
.. tags: nikola, git, blog
.. category:
.. link:
.. description: Show the steps how to create or modify an oA blog post
.. type: text
.. author: Volker Theile

If you want to create or modify a oA blog post, then this short introduction
will show you how to do this without the need to read howto's about Nikola.
You will be able to setup a virtual environment within seconds to avoid
polluting your Python installation and your Git repository on your development
system.

.. TEASER_END

Install the virtual environment package::

	# apt-get install virtualenv

Go to your previously cloned openatticblog Git repository and create the
virtual environment and run nikola::

	$ virtualenv -p python3 venv
	$ cd venv
	$ source bin/activate
	$ pip install -r ../requirements.txt

If you want to create a new blog post you need to enter::

	$ git checkout -b <NAME_OF_BRANCH>
	$ nikola new_post -a "<YOUR_NAME>" -t "Title of the blog post"
	$ git add posts/title-of-the-blog-post.rst

To check your changes simply start nikola. The blog will be updated
automatically if there are any changes::

	$ nikola auto

The blog is now available under http://localhost:8000.

Finally commit and push your changes if the post is ready to publish. Don't
forget to sign off the commit::

	$ git commit -a -s
	$ git push
