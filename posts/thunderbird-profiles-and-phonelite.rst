.. title: Thunderbird Profiles and PhoneLite
.. slug: thunderbird-profiles-and-phonelite
.. date: 2016-07-11 16:12:19 UTC+02:00
.. tags: mail, voip, thunderbird, 
.. category: 
.. link: 
.. description: 
.. type: text
.. author: Kai Wagner

I have found two really useful things this morning.

1. I didn't know that I can configure profiles within Thunderbird. Just start Thunderbird with -p to create several profiles::
	
	thunderbird -p 

2. With `PhoneLite <http://phonerlite.de/index_en.htm>`_ it's possible to connect to your Fritzbox and use all the "phone" functionality at your computer. Now there's no need for a mobile device at the home office any more. 
