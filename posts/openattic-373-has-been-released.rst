.. title: openATTIC 3.7.3 has been released
.. slug: openattic-373-has-been-released
.. date: 2019-07-18 14:33:20 UTC+02:00
.. tags: announcement, ceph, collaboration, contributing, news, opensource, release
.. category: 
.. link: 
.. description: Announcing the openATTIC 3.7.3 release
.. type: text
.. author: Laura Paduano

We're happy to announce version 3.7.3 of openATTIC!

Version 3.7.3 is the third release in 2019 and comes with various bugfixes.
We also included a security fix for a better session handling
and for blocking clickjacking attacks.
Furthermore we did some refactoring as well as a few improvements
of the openATTIC tests and their stability.

.. TEASER_END

Your feedback, ideas and bug reports are very welcome. If you would like to
get in touch with us, consider joining our
`openATTIC Users <https://groups.google.com/forum/#%21forum/openattic-users>`_
Google Group, visit our `#openattic <irc://irc.freenode.org/#openattic>`_
channel on
`irc.freenode.net <http://webchat.freenode.net/?channels=openattic>`_
or leave comments below this blog post.

See the list below for a more detailed change log and further references. The
OP codes in brackets refer to individual Jira issues that provide additional
details on each item. You can review these on our
`public Jira instance <http://tracker.openattic.org/>`_.

Changelog for version 3.7.3
---------------------------

Changed
~~~~~~~
* Improve session handling security and block clickjacking attacks 
  (:issue:`OP-3193`)

Fixed
~~~~~
* WebUI/QA: ISO 8601 timestamps in task queue unit tests (:issue:`OP-3194`)
* WebUI/QA: Add browser sleep to iSCSI suite (:issue:`OP-3190`)
* WebUI/QA: Refactored iSCSI suite (:issue:`OP-3191`)
* WebUI: Data pool dropdown is not set when editing RBD image with dedicated
  replicated data pool (:issue:`OP-3198`)
* Backend: Return UTC timestamp for RBD snapshots (:issue:`OP-3192`)
* Backend: iSCSI: add support for "backstore_emulate_pr" lrbd setting (:issue:`OP-3195`)
* Backend: RBD image update fails when RBD image uses a dedicated data pool (:issue:`OP-3197`)
