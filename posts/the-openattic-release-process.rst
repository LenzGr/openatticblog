.. title: The openATTIC release process
.. slug: the-openattic-release-process
.. date: 2017-10-12 10:00:00 UTC+02:00
.. tags: release, management, processes, team, collaboration
.. category: release management
.. link: 
.. description: 
.. type: text
.. author: Tatjana Dehler

Recently, I was responsible for the release of `openATTIC 3.5.1
<https://www.openattic.org/posts/openattic-351-has-been-released/>`_ and found
myself in the position of being the so called "Release Captain". In the following
blog post I'm going to explain the release process and the meaning of the
openATTIC Release Captain.

We release a new version of openATTIC every few weeks. To keep the quality of
the released version as high as possible and to make sure that the user
doesn’t run into obvious issues, the release process needs to be well
structured and repeatable.

.. TEASER_END

Therefore, someone of our team becomes the Release
Captain for the release. He or she is responsible for the whole release
process, organizes it, makes decisions and cares about keeping deadlines. By
rotating the Release Captain position through the team we assure that everyone
of us knows the release process and is able to ship a new version of openATTIC.

An openATTIC release is usually structured into three different steps:

#. Release preparations

   The first step of the release process is mainly focused on the preparation.
   Which means that the Release Captain decides together with the rest of the
   team which issues should go into the release. After that he or she sets a
   merge deadline and plans time for testing. This step also includes
   installation tests and manual GUI tests.

   The rest of the openATTIC team can support the Release Captain during this
   step by:

   * Creating `pull requests
     <https://bitbucket.org/openattic/openattic/pull-requests/>`_ as early as
     possible so that others have enough time for reviewing and testing them.
   * Keeping pull requests up to date. It doesn’t make any sense to test pull
     requests that are based on an outdated code base.

     You can easily keep your fork in sync with the openATTIC repository by
     using `"git rebase"`. I assume you've configured two remote repositories
     `"upstream"` and `"origin"` as you can see in the following example of the
     openATTIC blog repository::

       $ git remote -v
         origin	git@bitbucket.org/<user>/openatticblog.git (fetch)
         origin	git@bitbucket.org/<user>/openatticblog.git (push)
         upstream	git@bitbucket.org:openattic/openatticblog.git (fetch)
         upstream	git@bitbucket.org:openattic/openatticblog.git (push)

     `"upstream"` means the official openATTIC blog repository and `"origin"`
     is your own fork. If you want to update your branch `"new-post"` with the
     current state of the openATTIC blog `"master"` branch follow these steps:

     Get the latest changes from the `"upstream"` repository::

       $ git fetch upstream

     Switch to your `"new-post"` branch::

       $ git checkout new-post

     Update your `"new-post"` branch with the latest changes from the
     `"upstream"` repository::

       $ git rebase upstream/master

     Push the update to your remote fork::

       $ git push origin new-post -f

     **Note**: Be really really careful with using the `"-f"` option of the
     `"git push"` command and make sure that it's only used for pushing
     changes to your own repository fork. The `"-f"` option means **force**
     and might rewrite the history of the repository.

   * Merging pull requests as soon as possible. If a pull request is reviewed
     and tested one should merge them (unless there are no other dependencies
     you have to wait for – e.g. other pull requests to be merged). It prevents
     the pull request from being outdated and speeds up the release process.
   * Keeping `Jira <https://tracker.openattic.org>`_ up to date.
   * Not adding any new issues to the current release version after the merge
     deadline. The only exception: release blockers are still allowed to be
     added to the version after reaching the merge deadline. Nevertheless talk
     to the Release Captain before adding any new issues, please. Release
     blockers in general are:

     - Failures in the installation process itself.
     - Failures which will result in openATTIC not being usable at all.

#. The release build

   The release itself consists of a bunch of different steps. For example
   building a tarball out of the stable branch or creating packages on the
   `Open Build Service <https://build.opensuse.org/>`_. It also updates the
   `"version.txt"` file to the next version and sets the version tag on the
   openATTIC repository.

#. Post release

   After the release the Release Captain writes the announcement for the
   `blog <https://www.openattic.org/blog/>`_ and the `openATTIC Google Group
   <https://groups.google.com/forum/#!forum/openattic-users>`_. He also updates
   the openATTIC tracker. That means he finishes the current sprint (if
   applicable), creates the next version number (if it hasn’t been added yet)
   and starts the next sprint (if applicable).
